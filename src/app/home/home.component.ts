import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../auth/auth.service';
import { Router } from '@angular/router';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';

import {Observable} from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {


  private router: Router;
  public noteslist = [];
  private isloading: boolean;
  private isSelectedList = [];
  private canDelete: boolean;

  constructor(public auth: AuthService, private http: HttpClient, private _router: Router) {
    this.router = _router;
  }

  ngOnInit() {
    if (this.auth.isAuthenticated()) {
      this.apiGetNotesList();
    }
  }

  private toggleSelection(num): void {
    this.isSelectedList[num] =! this.isSelectedList[num];
    this.canDelete = false;
    for (let i in this.isSelectedList) {
      if (this.isSelectedList[i] == true)
        this.canDelete = true;
    };
  }

  private deleteSelected(): void {
    for (let i in this.isSelectedList) {
      console.log(i+"   "+this.isSelectedList[i]);
    };
  }

  public apiGetNotesList(): void {
    this.isloading = true;
    console.log("Calling with JWT: "+this.auth.idToken);
    this.http.get('https://api.athomson.co.nz/notes/v1/note', {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.auth.idToken}`)
    }).subscribe(res => {
      try {
        this.noteslist = JSON.parse((<any>res).body);
      } catch(e) {
        console.log(e);
      }
      console.log("Get Notes List Response:"+JSON.stringify(this.noteslist));
      this.isloading = false;
    }, err => {
      alert(err);
    })
  }

  public apiNewNote(): void {
    this.http.post('https://api.athomson.co.nz/notes/v1/note', {}, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.auth.idToken}`)
    }).subscribe(res => {
      console.log("New Note Response: "+JSON.stringify((<any>res)));
      this.router.navigateByUrl('/note/'+(<any>res).noteid);
    }, err => {
      alert(err);
    })
  }

}
