interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'jvg8tPpX1jzBdY1FpamklI4FWlX6ZINi',
  domain: 'athomson.auth0.com',
  // callbackURL: 'https://ed247339032446c5bf5825b6edd029b5.vfs.cloud9.us-east-2.amazonaws.com/callback'
  callbackURL: 'http://localhost:8080/callback'
};
