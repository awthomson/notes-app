import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './../auth/auth.service';
import { Router } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';

import {
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule ,
  MatStepperModule,
  MatInputModule
} from '@angular/material';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  constructor(private route: ActivatedRoute, public auth: AuthService, private http: HttpClient, private _router: Router) {
    this.router = _router;
  }

  ngOnInit() {
    console.log(this.route.snapshot.params['noteid']);
    this.apiGetNoteInfo();
  }

  router: Router;
  public isloading:boolean;
  public notetitle:string = "";
  public notebody:string = "";

  public apiGetNoteInfo(): void {
    this.isloading = true;
    var nid = this.route.snapshot.params['noteid'];
    this.http.get('https://api.athomson.co.nz/notes/v1/note/'+nid, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.auth.idToken}`)
    }).subscribe(res => {
      console.log("Get Note Info Response: "+JSON.stringify((<any>res).body));
      this.notebody = (<any>res).body.note;
      this.notetitle = (<any>res).body.title;
      this.isloading = false;
    })
  }

  public apiSaveNote(): void {
    var nid = this.route.snapshot.params['noteid'];
    this.http.post('https://api.athomson.co.nz/notes/v1/note/'+nid, {
      "title": this.notetitle, "note": this.notebody
    }, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.auth.idToken}`)
    }).subscribe(res => {
      console.log("Save Note Response: "+JSON.stringify(<any>res));
      this.router.navigateByUrl('/home');
    })
  }

  public apiDeleteNote(): void {
    var nid = this.route.snapshot.params['noteid'];
    this.http.delete('https://api.athomson.co.nz/notes/v1/note/'+nid, {
      headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.auth.idToken}`)
    }).subscribe(res => {
      console.log("Delete Response:"+JSON.stringify(<any>res));
      this.router.navigateByUrl('/home');
    })
  }

}
