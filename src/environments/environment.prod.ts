export const environment = {
  production: true,
  callback_url: "https://notes.athomson.co.nz/callback"
};
