#!/bin/bash

function test_1 {
echo "*** Getting list of all notes for user ***"
curl -s -o x.json -X GET "https://api.athomson.co.nz/notes/v1/note" -H "Authorization: Bearer ${TOKEN}"
jq . x.json
echo ""
}

function test_2 {
echo "*** Getting individual note ***"
curl -s -o x.json -X GET "https://api.athomson.co.nz/notes/v1/note/2" -H "Authorization: Bearer ${TOKEN}"
jq . x.json
echo ""
}

function test_3 {
echo "*** Update individual note ***"
curl -s -o x.json -X POST "https://api.athomson.co.nz/notes/v1/note/2" -H "Authorization: Bearer ${TOKEN}" -d '{"title": "ddd", "body": "nice body"}' -H "Content-Type: application/json"
jq . x.json
echo ""
}

function test_4 {
echo "*** Create new note ***"
curl -s -o x.json -X POST "https://api.athomson.co.nz/notes/v1/note" -H "Authorization: Bearer ${TOKEN}" -H "Content-Type: application/json"
jq . x.json
echo ""
}

test_4
